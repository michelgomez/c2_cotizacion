const http = require("http");
const express = require("express");
const bodyparser = require ("body-parser");

const app = express(); //OBJETO PRINCIPAL DE LA APLICACION
app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public')); 
app.use(bodyparser.urlencoded({extended:true}));

let datos =[{
    matricula: "2020030311",
    nombre: "Michelle Alejandra Valdiviezo Gomez",
    sexo: 'F',
    materias: ["Inglés","Base de Datos", "Tecnología I"]

},{
    matricula: "2020030609",
    nombre: "Yamileth Cristina Lopez Silvas",
    sexo: 'F',
    materias: ["Inglés","Base de Datos", "Tecnología I"]
}]

app.get("/", (req,res)=>{
    /res.send("Iniciando servidor 3000 para un nuevo servidor web");/
    res.render('index', {titulo: "Mi Primer Pagina en Embedded JavaScript",listado:datos, nombre: "Michelle Alejandra Valdiviezo Gomez", grupo: "8-3"});
})

app.get("/tabla",(req,res)=>{
    const params ={
        numero:req.query.numero
    }
    res.render('tabla', params);
})


app.post('/tabla',(req,res)=>{
    const params ={
        numero:req.body.numero
    }
    res.render('tabla', params);
})


app.get("/cotizacion",(req,res)=>{
    const params ={
        valor:req.query.valor,
        plazo:req.query.plazo,
        pinicial:req.query.pinicial
    }
    res.render('cotizacion', params);
})

app.post("/cotizacion",(req,res)=>{
    const params ={
        valor:req.body.valor,
        plazo:req.body.plazo,
        pinicial:req.body.pinicial
    }
    res.render('cotizacion', params);
})

// la pagina del error va al final de los get / post 
app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html')
})

const puerto = 3000;  
app.listen(puerto, ()=>{    //La aplicacion va a escuchar por el puerto 3000
    console.log("Iniciando puerto");
});

